+++
title = "About Omid"
slug = "about"
+++

Computers have been an obsession for me forever. Fabulous, logical, and at the beginning somehow magical.
FLOSS (Free/Libre/Open Source Software) Boom and the Second AI Spring were among the best things I could wish for and be able to see happen during my lifetime. Python and Artificial Intelligence paved the way for a change coming from the heart of the laboratories to the hobby weekend projects and brought the software development scene with its biggest change since the 1980s; a new Paradigm. Consequences of such a change place at the center of my work and interests. Developing tools that expand possibilities of the user either by bringing more efficient and quicker ways of doing trivial stuff or with shaping abilities unknown before.

Developing Statistical and Machine Learning models using python and its libraries, utilizing computer science Basics including Data Structures and Algorithms to favor More efficient and structured implementations, example-driven communication skills to explain complex technicals stuff to people who are more interested in the Practical Aspects of these matters in their Businesses and using software development methods to automate repeatable tasks are examples of my skills and interests.




