+++
title = "Contact"
slug = "contact"
+++

I don't like trying to have people's attention "for a second please". Even in the age of Twitter, real work is done in your Vim or VS Code window(one of the few good things Microsoft have ever made along with my Wireless Mouse 1000 :)) . For more on this matter including why and how; I would suggest having a look at Cal Newport's Best-Seller Deep Work.

Anyway, I'm well accessible through my email address and ready to hear if you got any suggestions or stuff you think may interest me (as listed on the about page). Here's my email address:

omidsaaedi+suggestion@gmail.com
